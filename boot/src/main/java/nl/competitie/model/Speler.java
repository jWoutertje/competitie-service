package nl.competitie.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "SPELERS")
public class Speler implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Column(name = "THUIS")
    private boolean thuis;

    private String naam;

    @ManyToOne
    private Wedstrijd wedstrijd;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Speler {\n");
        sb.append("    id: ").append(this.toIndentedString(this.id)).append("\n");
        sb.append("    thuis: ").append(this.toIndentedString(this.thuis)).append("\n");
//        sb.append("    deelnemerId: ").append(this.toIndentedString(this.deelnemerId)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    private String toIndentedString(Object o) {
        return o == null ? "null" : o.toString().replace("\n", "\n    ");
    }

}