package nl.competitie.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "DEELNEMERS")
public class Deelnemer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Column(name = "NAAM")
    private String naam;

    @Column(name = "GESPEELD")
    private int gespeeld = 0;

    @Column(name = "DOELSALDO")
    private int doelsaldo = 0;

    @Column(name = "GEWONNEN")
    private int gewonnen = 0;

    @ManyToOne
    private Competitie competitie;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Deelnemer {\n");
        sb.append("    id: ").append(this.toIndentedString(this.id)).append("\n");
        sb.append("    naam: ").append(this.toIndentedString(this.naam)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    private String toIndentedString(Object o) {
        return o == null ? "null" : o.toString().replace("\n", "\n    ");
    }

}