package nl.competitie.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "WEDSTRIJDEN")
public class Wedstrijd implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATUM")
    private OffsetDateTime datum;

    @OneToMany(cascade = {CascadeType.ALL},orphanRemoval=true)
    @JoinColumn(name = "WEDSTRIJD_ID")
    private List<Speler> spelers;

    @Column(name = "THUIS")
    private int scoreThuis;

    @Column(name = "UIT")
    private int scoreUit;

    @ManyToOne
    private Competitie competitie;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Wedstrijd {\n");
        sb.append("    id: ").append(this.toIndentedString(this.id)).append("\n");
        sb.append("    datum: ").append(this.toIndentedString(this.datum)).append("\n");
//        sb.append("    spelers: ").append(this.toIndentedString(this.spelers)).append("\n");
        sb.append("    scoreThuis: ").append(this.toIndentedString(this.scoreThuis)).append("\n");
        sb.append("    scoreUit: ").append(this.toIndentedString(this.scoreUit)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    private String toIndentedString(Object o) {
        return o == null ? "null" : o.toString().replace("\n", "\n    ");
    }


}