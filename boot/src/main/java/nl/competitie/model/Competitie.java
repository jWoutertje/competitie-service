package nl.competitie.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "COMPETITIES")
public class Competitie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Column(name = "NAAM")
    private String naam;

    @NotNull
    @ManyToOne
    @JoinColumn(name="COMPETITIETYPE_ID")
    private CompetitieType competitietype;

    @NotNull
    @Column(name = "TEAMGROOTTE")
    private int teamgrootte;

    @OneToMany(cascade = {CascadeType.ALL},orphanRemoval=true)
    @JoinColumn(name = "COMPETITIE_ID")
    private List<Wedstrijd> wedstrijden;

    @OneToMany(cascade = {CascadeType.ALL},orphanRemoval=true)
    @JoinColumn(name = "COMPETITIE_ID")
    private List<Deelnemer> deelnemers;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Competitie {\n");
        sb.append("    id: ").append(this.toIndentedString(this.id)).append("\n");
        sb.append("    naam: ").append(this.toIndentedString(this.naam)).append("\n");
//        sb.append("    wedstrijden: ").append(this.toIndentedString(this.wedstrijden)).append("\n");
//        sb.append("    deelnemers: ").append(this.toIndentedString(this.deelnemers)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    private String toIndentedString(Object o) {
        return o == null ? "null" : o.toString().replace("\n", "\n    ");
    }
}