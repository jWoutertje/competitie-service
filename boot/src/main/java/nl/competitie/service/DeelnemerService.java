package nl.competitie.service;

import nl.competitie.model.Competitie;
import nl.competitie.model.Deelnemer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeelnemerService extends AbstractService {

    public io.swagger.model.Deelnemer findOne(Long id) {
        Deelnemer deelnemerDao = deelnemerRepository.getOne(id);
        return convertToDto(deelnemerDao);
    }

    public void save(Long competitieId, List<io.swagger.model.Deelnemer> deelnemerDtos){
        Competitie competitie = competitieRepository.getOne(competitieId);
        for (io.swagger.model.Deelnemer deelnemerDto : deelnemerDtos) {
            Deelnemer deelnemer = convertToDao(deelnemerDto);
            deelnemer.setId(null);
            deelnemer.setCompetitie(competitie);
            deelnemerRepository.save(deelnemer);
        }
    }

    public void delete(Long id){
        Deelnemer deelnemer = deelnemerRepository.getOne(id);
        deelnemer.setCompetitie(null);
        deelnemerRepository.delete(deelnemer);
    }
}
