package nl.competitie.service;

class Wedstrijd {
    final Koppel thuis;
    final Koppel uit;

    Wedstrijd(Koppel thuis, Koppel uit) {
        this.thuis = thuis;
        this.uit = uit;
    }

    @Override
    public String toString() {
        return "(" + thuis.toString() + "-" + uit.toString() +")";
    }
}

