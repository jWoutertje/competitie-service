package nl.competitie.service;

import nl.competitie.model.*;
import nl.competitie.model.Wedstrijd;
import org.springframework.stereotype.Service;

@Service
public class WedstrijdService extends AbstractService {

    public io.swagger.model.Wedstrijd findOne(Long id) {
        Wedstrijd wedstrijdDao = wedstrijdRepository.getOne(id);
        return convertToDto(wedstrijdDao);
    }

    public io.swagger.model.Wedstrijd save(Long competitieId, io.swagger.model.Wedstrijd wedstrijdDto) {
        Wedstrijd wedstrijdDao = convertToDao(wedstrijdDto);
        Competitie competitieDao = competitieRepository.getOne(competitieId);
        wedstrijdDao.setCompetitie(competitieDao);

        for (Speler speler : wedstrijdDao.getSpelers()) {
            Deelnemer deelnemer = deelnemerRepository.findByCompetitieAndNaam(competitieDao, speler.getNaam());
            if (deelnemer != null) {
                updateSpeler(speler.isThuis(),wedstrijdDao.getScoreThuis(),wedstrijdDao.getScoreUit(),deelnemer);
            } else {
                //exception; speler moet deelnemer zijn.
            }
        }
        wedstrijdDao = wedstrijdRepository.save(wedstrijdDao);
        return convertToDto(wedstrijdDao);
    }

    public io.swagger.model.Wedstrijd update(Long competitieId, io.swagger.model.Wedstrijd wedstrijdDto) {
        Wedstrijd wedstrijdDao = wedstrijdRepository.getOne(wedstrijdDto.getId());
        wedstrijdDao.setDatum(wedstrijdDto.getDatum());
        wedstrijdDao.setScoreThuis(wedstrijdDto.getScoreThuis());
        wedstrijdDao.setScoreUit(wedstrijdDto.getScoreUit());
        Competitie competitieDao = competitieRepository.getOne(competitieId);

        for (Speler speler : wedstrijdDao.getSpelers()) {
            Deelnemer deelnemer = deelnemerRepository.findByCompetitieAndNaam(competitieDao, speler.getNaam());
            if (deelnemer != null) {
                updateSpeler(speler.isThuis(),wedstrijdDao.getScoreThuis(),wedstrijdDao.getScoreUit(),deelnemer);
            } else {
                //exception; speler moet deelnemer zijn.
            }
        }
        wedstrijdDao = wedstrijdRepository.save(wedstrijdDao);
        return convertToDto(wedstrijdDao);
    }

    public void delete(Long id){
        Wedstrijd wedstrijdDao = wedstrijdRepository.getOne(id);

        for (Speler speler : wedstrijdDao.getSpelers()) {
            speler.setWedstrijd(null);
            Deelnemer deelnemer = deelnemerRepository.findByCompetitieAndNaam(wedstrijdDao.getCompetitie(), speler.getNaam());
            if (deelnemer != null) {
                reverseUpdateSpeler(speler.isThuis(),wedstrijdDao.getScoreThuis(),wedstrijdDao.getScoreUit(),deelnemer);
            } else {
                //exception; speler moet deelnemer zijn.
            }
        }
        wedstrijdDao.setCompetitie(null);
        wedstrijdRepository.delete(wedstrijdDao);
    }

    private void updateSpeler(boolean thuis, int scoreThuis, int scoreUit, Deelnemer deelnemer) {
        int saldo = thuis ? scoreThuis - scoreUit : scoreUit - scoreThuis;
        deelnemer.setGespeeld(deelnemer.getGespeeld() + 1);
        deelnemer.setDoelsaldo(deelnemer.getDoelsaldo() + saldo);
        if(saldo > 0) {
            deelnemer.setGewonnen(deelnemer.getGewonnen() + 1);
        }
        deelnemerRepository.save(deelnemer);
    }

    private void reverseUpdateSpeler(boolean thuis, int scoreThuis, int scoreUit, Deelnemer deelnemer) {
        int saldo = thuis ? scoreThuis - scoreUit : scoreUit - scoreThuis;
        deelnemer.setGespeeld(deelnemer.getGespeeld() - 1);
        deelnemer.setDoelsaldo(deelnemer.getDoelsaldo() - saldo);
        if(saldo > 0) {
            deelnemer.setGewonnen(deelnemer.getGewonnen() - 1);
        }
        deelnemerRepository.save(deelnemer);
    }
}
