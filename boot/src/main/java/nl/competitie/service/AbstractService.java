package nl.competitie.service;

import nl.competitie.model.Competitie;
import nl.competitie.model.Deelnemer;
import nl.competitie.model.Wedstrijd;
import nl.competitie.repository.CompetitieRepository;
import nl.competitie.repository.CompetitieTypeRepository;
import nl.competitie.repository.DeelnemerRepository;
import nl.competitie.repository.WedstrijdRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class AbstractService {

    @Autowired
    protected CompetitieTypeRepository competitieTypeRepository;
    @Autowired
    protected CompetitieRepository competitieRepository;
    @Autowired
    protected WedstrijdRepository wedstrijdRepository;
    @Autowired
    protected DeelnemerRepository deelnemerRepository;

    protected Competitie convertToDao(io.swagger.model.Competitie competitieDto){
        return new ModelMapper()
                .map(competitieDto,Competitie.class);
    }

    protected io.swagger.model.Competitie convertToDto(Competitie competitieDao){
        return new ModelMapper()
                .map(competitieDao,io.swagger.model.Competitie.class);
    }

    protected List<io.swagger.model.Competitie> convertToDtoList(List<Competitie> competities){
        List<io.swagger.model.Competitie> competitieDtoList = new ArrayList<>();
        for (Competitie competitie : competities) {
            competitieDtoList.add(convertToDto(competitie));
        }
        return competitieDtoList;
    }

    protected Deelnemer convertToDao(io.swagger.model.Deelnemer deelnemerDto){
        return new ModelMapper()
                .map(deelnemerDto,Deelnemer.class);
    }

    protected io.swagger.model.Deelnemer convertToDto(Deelnemer deelnemerDao){
        return new ModelMapper()
                .map(deelnemerDao,io.swagger.model.Deelnemer.class);
    }

    protected Wedstrijd convertToDao(io.swagger.model.Wedstrijd wedstrijdDto){
        return new ModelMapper()
                .map(wedstrijdDto,Wedstrijd.class);
    }

    protected io.swagger.model.Wedstrijd convertToDto(Wedstrijd wedstrijdDao){
        return new ModelMapper()
                .map(wedstrijdDao,io.swagger.model.Wedstrijd.class);
    }
}
