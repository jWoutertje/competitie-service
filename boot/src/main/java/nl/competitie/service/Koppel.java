package nl.competitie.service;

class Koppel {
    final int persoon1;
    final int persoon2;

    Koppel(int persoon1, int persoon2) {
        this.persoon1 = persoon1;
        this.persoon2 = persoon2;
    }

    @Override
    public String toString() {
        return String.valueOf(persoon1) + "," + String.valueOf(persoon2);
    }
}

