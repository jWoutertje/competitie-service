package nl.competitie.service;

import nl.competitie.model.Competitie;
import nl.competitie.model.CompetitieType;
import nl.competitie.model.Speler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CompetitieService extends AbstractService {

    public io.swagger.model.Competitie findOne(Long id) {
        Competitie competitieDao = competitieRepository.getOne(id);
        return convertToDto(competitieDao);
    }

    public List<io.swagger.model.Competitie> findAll() {
        List<Competitie> competities = competitieRepository.findAll();
        return convertToDtoList(competities);
    }

    public io.swagger.model.Competitie save(io.swagger.model.Competitie competitieDto){
        CompetitieType competitieType = competitieTypeRepository.findByCompetitietype(competitieDto.getCompetitietype());
        Competitie competitieDao = convertToDao(competitieDto);
        competitieDao.getCompetitietype().setId(competitieType.getId());
        if (competitieDao.getCompetitietype().getCompetitietype().equals("Variabele teams")) {
            competitieDao = generateWedstrijden2(competitieDao);
        } else {
            competitieDao = generateWedstrijden1(competitieDao);
        }
        competitieDao = competitieRepository.save(competitieDao);
        return convertToDto(competitieDao);
    }

//    public io.swagger.model.Competitie update(io.swagger.model.Competitie competitieDto){
//        CompetitieType competitieType = competitieTypeRepository.findByCompetitietype(competitieDto.getCompetitietype());
//        Competitie competitieDao = convertToDao(competitieDto);
//        competitieDao.getCompetitietype().setId(competitieType.getId());
//        if (competitieDao.getCompetitietype().getCompetitietype().equals("Variabele teams")) {
//            competitieDao = generateWedstrijden2(competitieDao);
//        } else {
//            competitieDao = generateWedstrijden1(competitieDao);
//        }
//
//        competitieDao = competitieRepository.save(competitieDao);
//        return convertToDto(competitieDao);
//    }

    public void delete(Long id){
        Competitie competitie = competitieRepository.getOne(id);
        competitieRepository.delete(competitie);
    }

    public Competitie generateWedstrijden1(Competitie competitie) {
        int personen = competitie.getDeelnemers().size();
        List<Koppel> uniekeKoppels = getKoppels(personen);
        for (Koppel koppel : uniekeKoppels) {
            List<Speler> spelers = new ArrayList<>();
            Speler speler1 = new Speler();
            speler1.setThuis(true);
            speler1.setNaam(competitie.getDeelnemers().get(koppel.persoon1 - 1).getNaam());
            Speler speler2 = new Speler();
            speler2.setThuis(false);
            speler2.setNaam(competitie.getDeelnemers().get(koppel.persoon2 - 1).getNaam());

            spelers.add(speler1);
            spelers.add(speler2);

            nl.competitie.model.Wedstrijd wedstrijd = new nl.competitie.model.Wedstrijd();
            wedstrijd.setSpelers(spelers);
            competitie.getWedstrijden().add(wedstrijd);
        }
        return competitie;
    }

    public Competitie generateWedstrijden2(Competitie competitie) {
        int personen = competitie.getDeelnemers().size();
        List<Koppel> uniekeKoppels = getKoppels(personen);

        HashMap<Koppel,List<Koppel>> mogelijkeTegenstanders = new HashMap<>();
        for (Koppel koppel : uniekeKoppels) {
            mogelijkeTegenstanders.put(koppel,mogelijkeTegenstandersVoorKoppel(koppel,uniekeKoppels));
        }

        List<Wedstrijd> wedstrijden = new ArrayList<>();
        for (int i = 1;i <= uniekeKoppels.size()/2; i++) {
            Koppel thuisKoppel = koppelMetMinsteOpties(mogelijkeTegenstanders);
            Koppel uitKoppel = mogelijkeTegenstanders.get(thuisKoppel).get(0);
            wedstrijden.add(new Wedstrijd(thuisKoppel,uitKoppel));

            mogelijkeTegenstanders.remove(thuisKoppel);
            mogelijkeTegenstanders.remove(uitKoppel);

            mogelijkeTegenstanders.forEach((k, v) -> {
                v.remove(thuisKoppel);
                v.remove(uitKoppel);
            });
        }

        for (Wedstrijd wedstrijd : wedstrijden) {
            List<Speler> spelers = new ArrayList<>();

            Speler speler1 = new Speler();
            speler1.setThuis(true);
            speler1.setNaam(competitie.getDeelnemers().get(wedstrijd.thuis.persoon1 - 1).getNaam());

            Speler speler2 = new Speler();
            speler2.setThuis(true);
            speler2.setNaam(competitie.getDeelnemers().get(wedstrijd.thuis.persoon2 - 1).getNaam());

            Speler speler3 = new Speler();
            speler3.setThuis(false);
            speler3.setNaam(competitie.getDeelnemers().get(wedstrijd.uit.persoon1 - 1).getNaam());

            Speler speler4 = new Speler();
            speler4.setThuis(false);
            speler4.setNaam(competitie.getDeelnemers().get(wedstrijd.uit.persoon2 - 1).getNaam());

            spelers.add(speler1);
            spelers.add(speler2);
            spelers.add(speler3);
            spelers.add(speler4);

            nl.competitie.model.Wedstrijd wedstrijd1 = new nl.competitie.model.Wedstrijd();
            wedstrijd1.setSpelers(spelers);
            competitie.getWedstrijden().add(wedstrijd1);
        }
        return competitie;
    }


    private List<Koppel> getKoppels(int p) {
        List<Koppel> uniekeKoppels = new ArrayList<>();
        for (int i = 1; i <= p; i++) {
            for (int j = 1; j <= p; j++) {
                if (j > i) {
                    if (j != i) {
                        uniekeKoppels.add(new Koppel(i, j));
                    }
                }
            }
        }
        return uniekeKoppels;
    }
    private List<Koppel> mogelijkeTegenstandersVoorKoppel(Koppel koppel, List<Koppel> uniekeKoppels) {
        List<Koppel> tegenstanders = new ArrayList<>();
        for (Koppel uniekKoppel : uniekeKoppels) {
            if (koppel.persoon1 != uniekKoppel.persoon1
                    && koppel.persoon1 != uniekKoppel.persoon2
                    && koppel.persoon2 != uniekKoppel.persoon1
                    && koppel.persoon2 != uniekKoppel.persoon2) {
                tegenstanders.add(uniekKoppel);
            }
        }
        return tegenstanders;
    }

    private Koppel koppelMetMinsteOpties(HashMap<Koppel, List<Koppel>> koppels) {
        int minValueSize = 0;
        Koppel minKey = null;
        for (Map.Entry<Koppel, List<Koppel>> entry : koppels.entrySet()) {
            if (minValueSize == 0) {
                minValueSize = entry.getValue().size();
            }
            minValueSize = (entry.getValue().size() < minValueSize) ? entry.getValue().size() : minValueSize;
        }

        for (Map.Entry<Koppel, List<Koppel>> entry : koppels.entrySet()) {
            if (entry.getValue().size() == minValueSize) {
                minKey = entry.getKey();
            }
        }
        return minKey;
    }
}
