package nl.competitie.repository;

import nl.competitie.model.Competitie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitieRepository extends JpaRepository<Competitie,Long> {}
