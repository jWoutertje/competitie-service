package nl.competitie.repository;

import nl.competitie.model.Wedstrijd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WedstrijdRepository extends JpaRepository<Wedstrijd,Long> {}
