package nl.competitie.repository;

import nl.competitie.model.Competitie;
import nl.competitie.model.Deelnemer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeelnemerRepository extends JpaRepository<Deelnemer,Long> {
    Deelnemer findByCompetitieAndNaam(Competitie competitie, String naam);
}
