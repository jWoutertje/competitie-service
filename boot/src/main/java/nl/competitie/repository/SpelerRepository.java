package nl.competitie.repository;

import nl.competitie.model.Speler;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpelerRepository extends JpaRepository<Speler,Long> {
}
