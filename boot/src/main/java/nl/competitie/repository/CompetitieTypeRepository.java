package nl.competitie.repository;

import nl.competitie.model.CompetitieType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitieTypeRepository extends JpaRepository<CompetitieType,Long> {
    public CompetitieType findByCompetitietype(String competitietype);
}
