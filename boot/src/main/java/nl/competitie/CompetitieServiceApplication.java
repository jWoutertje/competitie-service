package nl.competitie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompetitieServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompetitieServiceApplication.class, args);
	}
}
