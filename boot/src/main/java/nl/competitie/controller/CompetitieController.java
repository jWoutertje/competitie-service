package nl.competitie.controller;

import io.swagger.annotations.ApiParam;
import io.swagger.api.CompetitiesApi;
import io.swagger.model.Competitie;
import io.swagger.model.Deelnemer;
import io.swagger.model.Wedstrijd;
import nl.competitie.service.CompetitieService;
import nl.competitie.service.DeelnemerService;
import nl.competitie.service.WedstrijdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = {"http://www.kntvb.nl", "https://www.kntvb.nl", "https://competitietje-client.herokuapp.com", "http://localhost:8080", "http://localhost:4200", })
public class CompetitieController implements CompetitiesApi {

    @Autowired
    private CompetitieService competitieService;
    @Autowired
    private WedstrijdService wedstrijdService;
    @Autowired
    private DeelnemerService deelnemerService;

    @Override
    public ResponseEntity<Competitie> addCompetitie(
            @ApiParam(value = "" ,required=true ) @Valid @RequestBody Competitie body) {
        body.setId(null);
        Competitie competitie = competitieService.save(body);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(competitie.getId()).toUri();

        return ResponseEntity.created(location).body(competitie);
    }

    @Override
    public ResponseEntity<Void> deleteCompetitie(
            @ApiParam(value = "",required=true) @PathVariable("competitieId") Integer competitieId) {
        Competitie competitie = competitieService.findOne(competitieId.longValue());
        if(competitie == null) {
            return ResponseEntity.notFound().build();
        }

        competitieService.delete(competitieId.longValue());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Competitie> getCompetitieById(
            @ApiParam(value = "",required=true) @PathVariable("competitieId") Integer competitieId) {
        Competitie competitie = competitieService.findOne(competitieId.longValue());
        if(competitie == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(competitie);
    }

    @Override
    public ResponseEntity<List<Competitie>> getCompetities() {
        return ResponseEntity.ok(competitieService.findAll());
    }

//    @Override
//    public ResponseEntity<Competitie> updateCompetitie(
//            @ApiParam(value = "",required=true) @PathVariable("competitieId") Integer competitieId,
//            @ApiParam(value = "" ,required=true ) @Valid @RequestBody Competitie body) {
//        body.setId(competitieId.longValue());
//
//        if(competitieService.findOne(competitieId.longValue()) == null) {
//            return ResponseEntity.notFound().build();
//        }
//
//        Competitie competitie = competitieService.update(body);
//
//        return ResponseEntity.ok(competitie);
//    }

    @Override
    public ResponseEntity<Void> addDeelnemers(
            @ApiParam(value = "",required=true) @PathVariable("competitieId") Integer competitieId,
            @ApiParam(value = "" ,required=true ) @Valid @RequestBody List<Deelnemer> body) {

        Competitie competitie = competitieService.findOne(competitieId.longValue());
        if(competitie == null) {
            return ResponseEntity.notFound().build();
        }

        deelnemerService.save(competitieId.longValue(), body);

        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteDeelnemer(
            @ApiParam(value = "",required=true) @PathVariable("competitieId") Integer competitieId,
            @ApiParam(value = "",required=true) @PathVariable("deelnemerId") Integer deelnemerId) {
        Competitie competitie = competitieService.findOne(competitieId.longValue());
        Deelnemer deelnemer = deelnemerService.findOne(deelnemerId.longValue());

        if(competitie == null || deelnemer == null) {
            return ResponseEntity.notFound().build();
        }

        deelnemerService.delete(deelnemerId.longValue());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Wedstrijd> addWedstrijd(
            @ApiParam(value = "",required=true) @PathVariable("competitieId") Integer competitieId,
            @ApiParam(value = "" ,required=true )  @Valid @RequestBody Wedstrijd body) {
        body.setId(null);

        Competitie competitie = competitieService.findOne(competitieId.longValue());
        if(competitie == null) {
            return ResponseEntity.notFound().build();
        }

        Wedstrijd wedstrijd = wedstrijdService.save(competitieId.longValue(), body);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(wedstrijd.getId()).toUri();

        return ResponseEntity.created(location).body(wedstrijd);
    }

    @Override
    public ResponseEntity<Wedstrijd> updateWedstrijd(
            @ApiParam(value = "",required=true) @PathVariable("competitieId") Integer competitieId,
            @ApiParam(value = "",required=true) @PathVariable("wedstrijdId") Integer wedstrijdId,
            @ApiParam(value = "" ,required=true )  @Valid @RequestBody Wedstrijd body) {

        Competitie competitie = competitieService.findOne(competitieId.longValue());
        Wedstrijd wedstrijd = wedstrijdService.findOne(wedstrijdId.longValue());
        if(competitie == null || wedstrijd == null){
            return ResponseEntity.notFound().build();
        }

        wedstrijd = wedstrijdService.update(competitieId.longValue(), body);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(wedstrijd.getId()).toUri();

        return ResponseEntity.created(location).body(wedstrijd);
    }

    @Override
    public ResponseEntity<Wedstrijd> getWedstrijdById(
            @ApiParam(value = "",required=true) @PathVariable("competitieId") Integer competitieId,
            @ApiParam(value = "",required=true) @PathVariable("wedstrijdId") Integer wedstrijdId) {
        Competitie competitie = competitieService.findOne(competitieId.longValue());
        Wedstrijd wedstrijd = wedstrijdService.findOne(wedstrijdId.longValue());
        if(competitie == null || wedstrijd == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(wedstrijd);
    }

        @Override
    public ResponseEntity<Void> deleteWedstrijd(
            @ApiParam(value = "",required=true) @PathVariable("competitieId") Integer competitieId,
            @ApiParam(value = "",required=true) @PathVariable("wedstrijdId") Integer wedstrijdId) {

        Competitie competitie = competitieService.findOne(competitieId.longValue());
        Wedstrijd wedstrijd = wedstrijdService.findOne(wedstrijdId.longValue());

        if(competitie == null || wedstrijd == null) {
            return ResponseEntity.notFound().build();
        }

        wedstrijdService.delete(wedstrijdId.longValue());
        return ResponseEntity.ok().build();
    }
}