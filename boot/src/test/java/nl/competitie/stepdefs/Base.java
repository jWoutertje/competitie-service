package nl.competitie.stepdefs;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.Competitie;
import io.swagger.model.Deelnemer;
import io.swagger.model.Wedstrijd;
import nl.competitie.CompetitieServiceApplication;
import nl.competitie.repository.CompetitieRepository;
import nl.competitie.repository.DeelnemerRepository;
import nl.competitie.repository.SpelerRepository;
import nl.competitie.repository.WedstrijdRepository;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = CompetitieServiceApplication.class)
@AutoConfigureMockMvc
public class Base {

    @Autowired
    protected ObjectMapper mapper;
    @Autowired
    protected CompetitieRepository competitieRepository;
    @Autowired
    protected DeelnemerRepository deelnemerRepository;
    @Autowired
    protected WedstrijdRepository wedstrijdRepository;
    @Autowired
    protected SpelerRepository spelerRepository;

    @Autowired
    protected MockMvc mockMvc;
    protected static List<Competitie> reqCompetities;
    protected static List<Deelnemer> reqDeelnemers;
    protected static List<Wedstrijd> reqWedstrijden;
    protected static ResultActions response;
    protected static List<Competitie> resCompetities;
    protected static List<Deelnemer> resDeelnemers;
    protected static List<Wedstrijd> resWedstrijden;
}
