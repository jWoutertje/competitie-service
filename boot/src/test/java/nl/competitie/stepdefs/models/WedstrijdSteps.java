package nl.competitie.stepdefs.models;

import cucumber.api.java.nl.Gegeven;
import io.swagger.model.Speler;
import io.swagger.model.Wedstrijd;
import nl.competitie.stepdefs.Base;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class WedstrijdSteps extends Base {

    @Gegeven("^een wedstrijd$")
    public void wedstrijd() {
        List<Speler> spelers = new ArrayList<>();
        spelers.add(new Speler().naam("SpelerThuis").thuis(true));
        spelers.add(new Speler().naam("SpelerUit").thuis(false));

        reqWedstrijden.add(new Wedstrijd()
                .scoreThuis(10)
                .scoreUit(9)
                .spelers(spelers));
    }
}

