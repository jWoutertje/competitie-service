package nl.competitie.stepdefs.models;

import cucumber.api.java.nl.Gegeven;
import io.swagger.model.Deelnemer;
import nl.competitie.stepdefs.Base;

public class DeelnemerSteps extends Base {

    @Gegeven("^een deelnemer$")
    public void deelnemer() {
        reqDeelnemers.add(new Deelnemer()
                .naam("Deelnemer"));
    }

    @Gegeven("^een deelnemer met naam '(.*)'$")
    public void deelnemerMetNaam(String naam) {
        reqDeelnemers.add(new Deelnemer()
                .naam(naam));
    }
}

