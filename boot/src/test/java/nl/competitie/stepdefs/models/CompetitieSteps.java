package nl.competitie.stepdefs.models;

import cucumber.api.java.nl.Gegeven;
import io.swagger.model.Competitie;
import nl.competitie.stepdefs.Base;

public class CompetitieSteps extends Base {

    @Gegeven("^een competitie$")
    public void competitie() {
        reqCompetities.add(new Competitie()
                .naam("Competitie")
                .teamgrootte(2));
    }

    @Gegeven("^een competitie met naam '(.*)'$")
    public void competitie(String naam) {
        reqCompetities.add(new Competitie()
                .naam(naam)
                .teamgrootte(2));
    }
}

