package nl.competitie.stepdefs;

import com.fasterxml.jackson.core.type.TypeReference;
import cucumber.api.java.nl.Dan;
import io.swagger.model.Competitie;
import io.swagger.model.Deelnemer;
import io.swagger.model.Wedstrijd;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ResponseSteps extends Base {

    @Dan("^krijg ik een http status '(\\d+)'$")
    public void assertStatusOk(int responseStatus) throws Throwable {
        response.andExpect(status().is(responseStatus));
    }

    @Dan("^krijg ik een competitie terug")
    public void assertCompetitie() throws Throwable {
        resCompetities.add(mapper.readValue(
                response.andReturn().getResponse().getContentAsString(),
                Competitie.class));
        assertTrue(resCompetities.get(resCompetities.size()-1).getId() > 0);
    }

    @Dan("^krijg ik een een lijst met '(.*)' competities terug")
    public void assertCompetitie(int aantal) throws Throwable {
        resCompetities = mapper.readValue(
                response.andReturn().getResponse().getContentAsString(),
                new TypeReference<List<Competitie>>(){});
        assertEquals(resCompetities.size(), aantal);
    }

    @Dan("^krijg ik een deelnemer terug")
    public void assertDeelnemer() throws Throwable {
        resDeelnemers.add(mapper.readValue(
                response.andReturn().getResponse().getContentAsString(),
                Deelnemer.class));
        assertTrue(resDeelnemers.get(resDeelnemers.size()-1).getId() > 0);
    }

    @Dan("^krijg ik een wedstrijd terug")
    public void assertWedstrijd() throws Throwable {
        resWedstrijden.add(mapper.readValue(
                response.andReturn().getResponse().getContentAsString(),
                Wedstrijd.class));
        assertTrue(resWedstrijden.get(resWedstrijden.size()-1).getId() > 0);
    }
}
