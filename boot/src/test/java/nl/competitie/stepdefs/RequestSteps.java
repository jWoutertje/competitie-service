package nl.competitie.stepdefs;

import cucumber.api.java.nl.Als;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

public class RequestSteps extends Base {

    private final String urlCompetities = "/api/competities";
    private final String urlDeelnemers = "/api/competities/%s/deelnemers";
    private final String urlWedstrijden = "/api/competities/%s/wedstrijden";


    @Als("^ik de competitie aanmaak$")
    public void postCompetitie() throws Exception {
        response = mockMvc.perform(post(urlCompetities)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(reqCompetities.get(reqCompetities.size()-1))));
    }

    @Als("^ik alle competities opvraag$")
    public void getCompetities() throws Throwable {
        response = mockMvc.perform(get(urlCompetities));
    }

    @Als("^ik de competitie opvraag$")
    public void getCompetitie() throws Throwable {
        Long id = resCompetities.get(resCompetities.size()-1).getId();
        response = mockMvc.perform(get(urlCompetities + '/' + id));
    }

    @Als("^ik de competitie verwijder$")
    public void deleteCompetitie() throws Throwable {
        Long id = resCompetities.get(resCompetities.size()-1).getId();
        response = mockMvc.perform(delete(urlCompetities + '/' + id));
    }

    @Als("^ik de deelnemer aanmaak$")
    public void postDeelnemer() throws Exception {
        Long id = resCompetities.get(resCompetities.size()-1).getId();
        String url = String.format(urlDeelnemers, id);
        String body = mapper.writeValueAsString(reqDeelnemers.get(reqDeelnemers.size()-1));
        response = mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body));
    }

    @Als("^ik de deelnemer verwijder$")
    public void deleteDeelnemer() throws Exception {
        Long competitieId = resCompetities.get(resCompetities.size()-1).getId();
        Long deelnemerId = resDeelnemers.get(resDeelnemers.size()-1).getId();
        String url = String.format(urlDeelnemers, competitieId);
        response = mockMvc.perform(delete(url + '/' + deelnemerId));
    }

    @Als("^ik de wedstrijd aanmaak$")
    public void postWedstrijd() throws Exception {
        Long id = resCompetities.get(resCompetities.size()-1).getId();
        String url = String.format(urlWedstrijden, id);
        String body = mapper.writeValueAsString(reqWedstrijden.get(reqWedstrijden.size()-1));
        response = mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body));
    }

    @Als("^ik de wedstrijd verwijder$")
    public void deleteWedstrijd() throws Exception {
        Long competitieId = resCompetities.get(resCompetities.size()-1).getId();
        Long wedstrijdId = resWedstrijden.get(resWedstrijden.size()-1).getId();
        String url = String.format(urlWedstrijden,competitieId);
        response = mockMvc.perform(delete(url + '/' + wedstrijdId));
    }
}

