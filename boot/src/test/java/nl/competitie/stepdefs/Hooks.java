package nl.competitie.stepdefs;

import cucumber.api.java.Before;

import java.util.ArrayList;

public class Hooks extends Base {

    @Before
    public void clean() {
        System.out.print("\n \n BEFORE HOOK \n \n");
        reqCompetities = new ArrayList<>();
        reqWedstrijden = new ArrayList<>();
        reqDeelnemers = new ArrayList<>();
        resCompetities = new ArrayList<>();
        resWedstrijden = new ArrayList<>();
        resDeelnemers = new ArrayList<>();
        response = null;
        wedstrijdRepository.deleteAll();
        spelerRepository.deleteAll();
        deelnemerRepository.deleteAll();
        competitieRepository.deleteAll();
    }
}