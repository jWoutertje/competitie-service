#language: nl
Functionaliteit: Deelnemer aanmaken

  Achtergrond:
    Gegeven een competitie
    Als ik de competitie aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een competitie terug

  Scenario: Deelnemer aanmaken
    Gegeven een deelnemer
    Als ik de deelnemer aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een deelnemer terug