#language: nl
Functionaliteit: Wedstrijd verwijderen

  Achtergrond:
    Gegeven een competitie
    Als ik de competitie aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een competitie terug
    Gegeven een deelnemer met naam 'SpelerThuis'
    Als ik de deelnemer aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een deelnemer terug
    Gegeven een deelnemer met naam 'SpelerUit'
    Als ik de deelnemer aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een deelnemer terug
    Gegeven een wedstrijd
    Als ik de wedstrijd aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een wedstrijd terug

  Scenario: Wedstrijd verwijderen
    Als ik de wedstrijd verwijder
    Dan krijg ik een http status '200'
