#language: nl
Functionaliteit: Deelnemer verwijderen

  Achtergrond:
    Gegeven een competitie
    Als ik de competitie aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een competitie terug
    Gegeven een deelnemer
    Als ik de deelnemer aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een deelnemer terug


  Scenario: Deelnemer verwijderen
    Als ik de deelnemer verwijder
    Dan krijg ik een http status '200'
