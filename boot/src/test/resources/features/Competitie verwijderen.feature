#language: nl
Functionaliteit: Competitie verwijderen

  Achtergrond:
    Gegeven een competitie
    Als ik de competitie aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een competitie terug

  Scenario: Competitie verwijderen
    Als ik de competitie verwijder
    Dan krijg ik een http status '200'
