#language: nl
Functionaliteit: Competitie aanmaken

  Scenario: Competitie aanmaken
    Gegeven een competitie
    Als ik de competitie aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een competitie terug
