#language: nl
Functionaliteit: Competitie opvragen

  Achtergrond:
    Gegeven een competitie
    Als ik de competitie aanmaak
    Dan krijg ik een http status '201'
    En krijg ik een competitie terug

  Scenario: Competitie opvragen
    Als ik de competitie opvraag
    Dan krijg ik een http status '200'
    En krijg ik een competitie terug
